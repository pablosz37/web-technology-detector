class DetectorController < ApplicationController
  def index
  end

  def out
    head = Nokogiri::HTML(open(params[:url])).xpath('//head')
    @count = head.first.to_s.scan(Regexp.new(params[:technology])).size 
  end
end
