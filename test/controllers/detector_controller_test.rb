require 'test_helper'

class DetectorControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get detector_index_url
    assert_response :success
  end

  test "should get out" do
    get detector_out_url
    assert_response :success
  end

end
