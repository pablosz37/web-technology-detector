Rails.application.routes.draw do
  get 'detector/index'

  post 'detector/out'

  root 'detector#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
